*------------------*---------------------*
| Column           | Variabel            |
*------------------|---------------------*
| id               | auto_increment      |
| nama             | in_nama             |
| NIK              | in_nik              |
| HP               | in_hp               |
| email            | in_email            |
| skema            | in_skema            |
| rekomendasi      | in_rekomendasi      |
| tanggal_terbit   | in_tgl_terbit       |
| tanggal_lahir    | in_tgl_lahir        |
| organisasi       | in_organisasi       |
*------------------*---------------------*
