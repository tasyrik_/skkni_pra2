/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.31-MariaDB : Database - skkni_pra2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`skkni_pra2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `skkni_pra2`;

/*Table structure for table `data` */

DROP TABLE IF EXISTS `data`;

CREATE TABLE `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `NIK` varchar(100) DEFAULT NULL,
  `HP` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `skema` varchar(100) DEFAULT NULL,
  `rekomendasi` varchar(100) DEFAULT NULL,
  `tanggal_terbit` date DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `organisasi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `data` */

insert  into `data`(`id`,`nama`,`NIK`,`HP`,`email`,`skema`,`rekomendasi`,`tanggal_terbit`,`tanggal_lahir`,`organisasi`) values 
(1,'allan','2344775778','75858','ffghv','ihghvmv','terbaik','2006-06-09','1994-06-15','unsrat'),
(2,'bebek','78968698','96799875','nvvmvgvnb','cfsdjfscdc','belajar','2009-09-10','1993-04-05','unsrit'),
(3,'eja','98676847687','67845687','jfdkfghyg','apa sto','pulang jo','2010-10-11','1993-05-06','ghsvfgv'),
(4,'will','846783654','086476587y','mhdjfs.com','empat tiga tiga','bodo amat','2010-11-12','2000-09-09','hahhahaha'),
(5,'alo','223131231','3232523','alamak','apa sto 2','wow pwkwkkw','2010-11-03','1993-05-06','wkwkwk');

/* Procedure structure for procedure `cetak_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `cetak_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `cetak_data`()
begin
select id,tanggal_lahir, count(*) AS jumlah FROM data;
END */$$
DELIMITER ;

/* Procedure structure for procedure `cetak_data_dua` */

/*!50003 DROP PROCEDURE IF EXISTS  `cetak_data_dua` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `cetak_data_dua`()
begin
select id,tanggal_lahir, count(*) AS jumlah FROM data group by tanggal_lahir;
END */$$
DELIMITER ;

/* Procedure structure for procedure `tampil_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `tampil_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `tampil_data`()
BEGIN
	select * from data group by tanggal_lahir;
END */$$
DELIMITER ;

/* Procedure structure for procedure `tampil_data_dua` */

/*!50003 DROP PROCEDURE IF EXISTS  `tampil_data_dua` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `tampil_data_dua`()
BEGIN
	select * from data group by tanggal_terbit;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
